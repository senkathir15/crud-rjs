import React from 'react'
import { useRef,useContext,useEffect } from 'react';
import { Form, Button, Card, Alert } from "react-bootstrap";
import { AuthContext } from '../context/authContext';
import axios from "axios";
import { Link, useHistory } from "react-router-dom";
import Table from "react-bootstrap/Table";

const product=[];

export default function Add() {
  //  const [error, setError] = React.useState("");
   const [image, setImage] = React.useState("");
   const { addProduct, seterror, item, deleteProduct } =
     useContext(AuthContext);
   const ProductNameRef = useRef();
   const priceRef = useRef();
   const descriptionRef = useRef();
   const imageRef = useRef();
   const value = {};

   useEffect(() => {}, [value]);
      const history = useHistory();
     const handelSubmit = (e) => {
     e.preventDefault();
      value["id"] = new Date().getTime().toString();
      value[ProductNameRef.current.name] = ProductNameRef.current.value;
      value[priceRef.current.name] = priceRef.current.value;
      value[descriptionRef.current.name] = descriptionRef.current.value;
       addProduct(value);
       ProductNameRef.current.value="";
        priceRef.current.value="";
        descriptionRef.current.value="";
   };

   return (
     <>
       <Card className="card m-auto">
         <Card.Body>
           <h2 className="text-center mb-4">Add product Details</h2>
           {/* {error && <Alert>{error}</Alert>} */}
           <Form onSubmit={handelSubmit}>
             <Form.Group className="mb-3" name="fristName">
               <Form.Label>
                 ProductName<span className="requried">*</span>
               </Form.Label>
               <Form.Control
                 name="name"
                 type="text"
                 placeholder="Enter productName"
                 ref={ProductNameRef}
                 required
               />
             </Form.Group>

             <Form.Group className="mb-3" controlId="passWord">
               <Form.Label>
                 price<span className="requried">*</span>
               </Form.Label>
               <Form.Control
                 type="number"
                 placeholder="Enter price"
                 required
                 ref={priceRef}
                 name="price"
               />
             </Form.Group>
             <Form.Group className="mb-3" controlId="passWord">
               <Form.Label>
                 Description<span className="requried">*</span>
               </Form.Label>
               <Form.Control
                 type="text"
                 placeholder="Enter Title"
                 ref={descriptionRef}
                 required
                 name="description"
               />
             </Form.Group>
             <div className="btns">
               <Button type="submit" variant="primary">
                 ADD
               </Button>
             </div>
           </Form>
           <div className="w-100 text-center mt-2"></div>
         </Card.Body>
       </Card>

       <Table striped bordered hover>
         <thead>
           <tr>
             <th>Product Name</th>
             <th>price</th>
             <th>Description</th>
             <th>Edit</th>
             <th>Delete</th>
           </tr>
         </thead>
         <tbody>
           {item.length > 0 ? item.map((val,index) => {
             return (
               <tr>
                 <td>{val.name}</td>
                 <td>{val.price}</td>
                 <td>{val.description}</td>
                 <td>
                   <Link to={`/edit/${index}`} key={index}>
                     <Button> edit</Button>
                   </Link>
                 </td>
                 <td>
                   <Button
                     variant="primary"
                     onClick={() => deleteProduct(val.id)}
                   >
                     Delete
                   </Button>
                 </td>
               </tr>
             );
           }) : <h1>No products list</h1>}
         </tbody>
       </Table>
     </>
   );
}

