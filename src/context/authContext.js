import React from "react";
import {
  useState,
  createContext,
} from "react";

import { useHistory } from "react-router-dom";
export const AuthContext = createContext();
  export function AuthProvider(prop){
      const history = useHistory();
    
      const [item, setItems] = useState([]);



    const addProduct = (data)=>{
      setItems((pre)=>[...pre,data])
} 
    const editProduct = (data,index) => {
       let upadteItem = [...item];
       upadteItem[index] = data;
       setItems(upadteItem);
        history.push("/");
    }; 

    const deleteProduct = (id) => {
       let upadteItem =[...item];
       upadteItem = upadteItem.filter((item) => item.id !== id);
       setItems(upadteItem);
    }; 
    const value = {
      item,
      addProduct,
      editProduct,
      deleteProduct,
    };

    return (
        <AuthContext.Provider value={value}>
          {prop.children}
        </AuthContext.Provider>
    )
}


