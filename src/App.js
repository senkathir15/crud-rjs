import { AuthProvider } from './context/authContext';
import {BrowserRouter as Router,Switch,Route } from "react-router-dom"
import Add from "./components/add";
import Edit from "./components/edit"

function App() {
  return (
    <Router>
      <AuthProvider>
        <Switch>
          <Route exact path="/" component={Add}></Route>
          <Route exact path="/edit/:id" component={Edit}></Route>
        </Switch>
      </AuthProvider>
    </Router>
  );
}
export default App;
